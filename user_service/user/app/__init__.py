import os
from flask import Flask
from .utils import initialize_db, initialize_encrypt, CustomJSONEncoder

app = Flask(__name__)

app.config['MONGO_URI'] = os.environ.get('MONGO_URI')
app.config['JWT_SECRET_KEY'] = os.environ.get('JWT_SECRET')

initialize_db(app)
initialize_encrypt(app)

app.json_encoder = CustomJSONEncoder

from .api.routes import user_api_blueprint
app.register_blueprint(user_api_blueprint)