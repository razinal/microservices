from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError
from flask_bcrypt import generate_password_hash, check_password_hash

auth_schema = {
    "type": "object",
    "properties": {
        "email": {
            "type": "string",
            "format": "email"
        },
        "password": {
            "type": "string",
            "minLength": 5
        }
    },
    "required": ["email", "password"],
    "additionalProperties": True
}

user_schema = {
    "type": "object",
    "required": ["email", "password"],
    "properties": {
        "username": {
            "type": "string"
        },
        "email": {
            "type": "string"
        },
        "firstname": {
            "type": "string"
        },
        "lastname": {
            "type": "string"
        },
        "password": {
            "type": "string"
        },
        "avatar": {
            "type": "string"
        },
        "authenticated": {
            "type": "boolean"
        },
        "location": {
            "type": "array",
            "items": { "$ref": "#/refLocation/longlat" }
        }
    },
    "refLocation": {
        "longlat": {
            "type": "object",
            "required": [ "longitude", "latitude" ],
            "properties": {
                "longitude":{
                    "type": "string",
                },
                "latitude": {
                    "type": "string",
                }
            }
        }
    },
    "additionalProperties": False
}

user_update_schema = {
    "type": "object",
    "required": ["email", "payload"],
    "properties": {
        "email": {
            "type": "string"
        },
        "payload": {
            "type": "object",
            "properties": {
                "username": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "avatar": {
                    "type": "string"
                },
                "firstname": {
                    "type": "string"
                },
                "lastname": {
                    "type": "string"
                },
                "alamat": {
                    "type": "string"
                },
                "complete": {
                    "type": "integer"
                },
                "password": {
                    "type": "string"
                },
                "hp": {
                    "type": "integer"
                },
                "jasasewa": {
                    "type": "boolean"
                },
                "location": {
                    "type": "array",
                    "items": { "$ref": "#/refLocation/longlat" }
                }
            },
            "additionalProperties": False
        },
        "refLocation": {
            "longlat": {
                "type": "object",
                "required": [ "longitude", "latitude" ],
                "properties": {
                    "longitude":{
                        "type": "string",
                    },
                    "latitude": {
                        "type": "string",
                    }
                }
            }
        },
    },
    "additionalProperties": False
}

# user_imgupdate_schema = {
#     "type": "object",
#     "required": ["id", "name"],
#     "properties": {
#         "id": {
#             "type": "string"
#         },
#         "name": {
#             "type": "string"
#         }
#     },
#     "additionalProperties": False
# }

location_schema = {
    "type": "object",
    "required": ["user_id","latitude","longitude"],
    "properties": {
        "user_id": {
            "type": "string"
        },
        "longitude": {
            "type": "string"
        },
        "latitude": {
            "type": "string"
        }
    },
    "additionalProperties": False
}

def validate_auth(data):
    try:
        validate(data, auth_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}

def validate_user(data):
    try:
        validate(data, user_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}

def validate_user_update(data):
    try:
        validate(data, user_update_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}

def validate_location(data):
    try:
        validate(data, location_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}

# def validate_imguser_update(data):
#     try:
#         validate(data, user_imgupdate_schema)
#     except ValidationError as e:
#         return {'_response': False, 'message': e}
#     except SchemaError as e:
#         return {'_response': False, 'message': e}
#     return {'_response': True, 'data': data}

def hash_password(password):
   encrypted = generate_password_hash(password).decode('utf8')
   return encrypted

def check_password(encrypted, password):
    return check_password_hash(encrypted, password)