import os
import datetime
from bson.objectid import ObjectId
from flask import Blueprint, request, jsonify
from flask_cors import CORS

from ..utils import mongo
from .schema import (
    validate_auth,
    validate_user, 
    validate_user_update, 
    # validate_imguser_update,
    hash_password,
    check_password
)

import logger

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(__name__, filename=os.path.join(ROOT_PATH, 'output.log'))

user_api_blueprint = Blueprint('user_api', __name__)
CORS(user_api_blueprint)

NOW = datetime.datetime.now()

@user_api_blueprint.route('/api/user/auth', methods=['POST'])
def auth_user():
    ''' auth endpoint '''
    validated = validate_auth(request.get_json())
    if validated['_response']:
        user = mongo.db.users.find_one({'email': validated['data']['email']}, {"_id": 0})
        # LOG.debug(user)
        print(user)
        print(user['password']+'==='+validated['data']['password'])
        print(hash_password(validated['data']['password']))
        print(check_password(user['password'], validated['data']['password']))
        if user and check_password(user['password'], validated['data']['password']):
            del user['password']
            return jsonify({'_response': True, 'message': 'success', 'token': validated['data']['access_token'], 'refresh': validated['data']['refresh_token']}), 200
        else:
            return jsonify({'_response': False, 'message': 'invalid email or password'}), 401
    else:
        return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400


@user_api_blueprint.route('/api/users/', methods=['GET'])
def users():
    """
    Function to fetch data.
    """
    try:
        user_collection = mongo.db.users
        records_fetched = [user for user in user_collection.find({})]  # show _id
        # records_fetched = [user for user in user_collection.find({}, {"_id":0})] ## hide _id
        return jsonify({'_response': True, 'users': records_fetched}), 200
    except:
        return jsonify({ '_response': False, 'data': 'Not Found'}), 400

@user_api_blueprint.route('/api/userid/', methods=['GET'])
def getuserbyid():
    data = request.get_json()
    user_collection = mongo.db.users
    try:
        if data.get('email', None) is not None:
            # user = user_collection.find_one_or_404({ '_id' : ObjectId(data['id']) })
            user = user_collection.find_one_or_404({ 'email' : data['email'] })
            user_id = {k:v for k, v in user.items() if k == '_id'}
            return jsonify({ '_response': True, 'user' : user_id }), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters'}), 400
    except:
        return jsonify({'_response': False, 'message': 'Data Not Found!'}), 400

@user_api_blueprint.route('/api/user/', methods=['GET', 'POST', 'DELETE', 'PATCH'])
def user():
    data = request.get_json()
    user_collection = mongo.db.users
    if request.method == 'GET':
        """
            Function to get the user from a user id.
        """
        try:
            if data.get('email', None) is not None:
                # user = user_collection.find_one_or_404({ '_id' : ObjectId(data['id']) })
                user = user_collection.find_one_or_404({ 'email' : data['email'] })
                user_id = {k:v for k, v in user.items() if k != '_id'}
                return jsonify({ '_response': True, 'user' : user_id }), 200
            else:
                return jsonify({'_response': False, 'message': 'Bad request parameters'}), 400
        except:
            return jsonify({'_response': False, 'message': 'Data Not Found!'}), 400

    if request.method == 'POST':
        """
            Function to create new user.
        """
        validated = validate_user(data)
        if validated['_response']:
            cekEmail = user_collection.find_one({'email': validated['data']['email']})
            if not cekEmail:
                validated['data']['password'] = hash_password(validated['data']['password'])            
                validated['data']['created_at'] = NOW
                validated['data']['location'] = [{"longitude": "0","latitude": "0"}]
                UserObjectId = ObjectId()

                user = user_collection.insert({
                    '_id': UserObjectId,
                    'email': validated['data']['email'],
                    'password': validated['data']['password'],
                })

                loc_collection = mongo.db.locations
                [ul.update({'user_id': UserObjectId}) for ul in validated['data']['location']]
                userLoc = loc_collection.insert(validated['data']['location'])
                return jsonify({'_response': True, 'message':'User creted successfully'}), 200
            return jsonify({'_response': False, 'message': 'Data exists'}), 400
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400

    if request.method == 'PATCH':
        """
            Function to update the user.
        """
        validated = validate_user_update(data)
        if validated['_response']:
            updateUser = user_collection.update({"email": validated['data']['email']}, {'$set': validated['data']['payload']})
            if updateUser['updatedExisting'] is False:
                return jsonify({ '_response' : False, 'result': updateUser }), 400
            return jsonify({'_response': True, 'message': 'user profile updated'}), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400

    if request.method == 'DELETE':
        """
            Function to remove the user.
        """
        if data.get('id', None) is not None:
            if ObjectId.is_valid(data['id']):
                deleteUser = user_collection.remove({"_id": ObjectId(data['id'])})
                if deleteUser['n'] == 1:
                    return jsonify({ '_response': True, 'deleted': data['id'] }), 200
                return jsonify({'_response': False, 'message': 'Data Not Found!'}), 400
            else:
                return jsonify({'_response': False, 'message': 'is not a valid ObjectId!'}), 400
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters!'}), 400


@user_api_blueprint.route("/")
@user_api_blueprint.route("/api/")
def get_initial__response():
    """Welcome message for the API."""
    message = {
        'apiVersion': 'v1.0',
        'status': '200',
        'message': 'Welcome to the User Service API'
    }

    resp = jsonify(message)
    return resp
