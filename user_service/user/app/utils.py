import json
import datetime
from bson.objectid import ObjectId
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt

mongo = PyMongo()
encrypt = Bcrypt()

def initialize_db(app):
    mongo.init_app(app)

def initialize_encrypt(app):
    encrypt.init_app(app)

class CustomJSONEncoder(json.JSONEncoder):
    ''' extend json-encoder class'''

    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)