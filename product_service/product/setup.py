import os
import sys

ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
os.environ.update({'ROOT_PATH': ROOT_PATH})
sys.path.append(os.path.join(ROOT_PATH, 'product'))

import logger
from app import app

LOG = logger.get_root_logger(os.environ.get('ROOT_LOGGER', 'root'), filename=os.path.join(ROOT_PATH, 'output.log'))
# PORT = os.environ.get('FLASK_PORT')

if __name__ == '__main__':
    LOG.info('running environment: %s', os.environ.get('FLASK_ENV'))
    app.config['DEBUG'] = os.environ.get('FLASK_ENV') == 'development'
    app.run(host='0.0.0.0')