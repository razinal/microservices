import json
import datetime
from bson.objectid import ObjectId
from flask_pymongo import PyMongo

mongo = PyMongo()


def initialize_db(app):
    mongo.init_app(app)


class CustomJSONEncoder(json.JSONEncoder):
    ''' extend json-encoder class'''

    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)