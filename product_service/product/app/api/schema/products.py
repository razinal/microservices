from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

product_schema = {
    "type": "object",
    "required": ["harga", "deskripsi", "image", "nama", "slug", "user_id"],
    "properties": {
        "nama": {
            "type": "string"
        },
        "slug": {
            "type": "string"
        },
        "deskripsi": {
            "type": "string"
        },
        "image": {
            "type": "array",
            "items": {"$ref": "#/embedImage/imageupload"}
        },
        "harga": {
            "type": "integer"
        },
        "user_id": {
            "type": "string"
        }
    },
    "embedImage": {
        "imageupload": {
            "type": "object",
            "required": ["nama"],
            "properties": {
                "nama": {
                    "type": "string",
                    "description": "The name of the image."
                }
            }
        }
    },
    "additionalProperties": True
}


product_update_schema = {
    "type": "object",
    "required": ["id", "payload"],
    "properties": {
        "id": {
            "type": "string"
        },
        "payload": {
            "type": "object",
            "properties": {
                "title": {
                    "type": "string"
                },
                "slug": {
                    "type": "string"
                },
                "deskripsi": {
                    "type": "string"
                },
                "harga": {
                    "type": "integer"
                }
            },
            "additionalProperties": False
        }
    },
    "additionalProperties": False
}

product_imgupdate_schema = {
    "type": "object",
    "required": ["id", "name"],
    "properties": {
        "id": {
            "type": "string"
        },
        "name": {
            "type": "string"
        }
    },
    "additionalProperties": False
}


categories_schema = {
    "type": "object",
    "required": ["name", "vector", "icon"],
    "properties": {
        "name": {
            "type": "string"
        },
        "vector": {
            "type": "string"
        },
        "icon": {
            "type": "string"
        }
    },
    "additionalProperties": False
}

update_categories_schema = {
    "type": "object",
    "required": ["id", "payload"],
    "properties": {
        "id": {
            "type": "string"
        },
        "payload": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "vector": {
                    "type": "string"
                },
                "icon": {
                    "type": "string"
                }
            },
            "additionalProperties": False
        }
    },
    "additionalProperties": False
}


def validate_category(data):
    try:
        validate(data, categories_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}


def validate_categories_update(data):
    try:
        validate(data, update_categories_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}


def validate_product(data):
    try:
        validate(data, product_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}


def validate_product_update(data):
    try:
        validate(data, product_update_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}


def validate_imgproduct_update(data):
    try:
        validate(data, product_imgupdate_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}
