import os
import datetime
from flask import Blueprint, request, jsonify
from flask_cors import CORS
from bson.objectid import ObjectId
from ..utils import mongo
from .schema import (
    validate_product, 
    validate_product_update, 
    validate_imgproduct_update, 
    validate_category,
    validate_categories_update
)

import logger

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(__name__, filename=os.path.join(ROOT_PATH, 'output.log'))

product_api_blueprint = Blueprint('product_api', __name__)
CORS(product_api_blueprint)

NOW = datetime.datetime.now()

@product_api_blueprint.route('/api/products', methods=['GET'])
def products():
    """
    Function to fetch data.
    """
    try:
        product_collection = mongo.db.products
        records_fetched = [product for product in product_collection.find().sort('created_at', -1)]  # show _id 
        # print(records_fetched)
        # records_fetched = [product for product in product_collection.find({}, {"_id":0})] ## hide _id
        return jsonify({'_response': True, 'products': records_fetched}), 200
    except:
        return jsonify({ '_response': False, 'data': 'Not Found'}), 400

@product_api_blueprint.route('/api/product/', methods=['GET', 'POST', 'DELETE', 'PATCH'])
def product():
    data = request.get_json()
    product_collection = mongo.db.products
    if request.method == 'GET':
        """
            Function to get the product from a slug.
        """
        try:
            if data.get('slug', None) is not None:
                product = product_collection.find_one_or_404({ 'slug' : data['slug'] })
                product_slug = {k:v for k, v in product.items() if k != '_id'}
                return jsonify({ '_response': True, 'product' : product_slug }), 200
            else:
                return jsonify({'_response': False, 'message': 'Bad request parameters'}), 400
        except:
            return jsonify({'_response': False, 'message': 'Data Not Found!'}), 400

    if request.method == 'POST':
        """
            Function to create new product.
        """
        validated = validate_product(data)
        if validated['_response']:
            # result = [{'elem':'value', **item} for item in data['data']['image']]
            validated['data']['user_id'] = ObjectId(validated['data']['user_id'])
            validated['data']['created_at'] = NOW

            for item in validated['data']['image']:
                item.update({'_id': ObjectId()})

            product = product_collection.insert(validated['data'])
            return jsonify({'_response': True, 'data': product}), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400

    if request.method == 'PATCH':
        """
            Function to update the product.
        """
        validated = validate_product_update(data)
        if validated['_response']:
            # product_collection.update_one({"_id": ObjectId(validated['data']['id'])}, {'$set': validated['data']['payload']})
            # return jsonify({'_response': True, 'message': 'record updated'}), 200
            updateProduct = product_collection.update({"_id": ObjectId(validated['data']['id'])}, {'$set': validated['data']['payload']})
            if updateProduct['updatedExisting'] is False:
                return jsonify({ '_response' : False, 'result': updateProduct }), 400
            return jsonify({'_response': True, 'message': 'product updated'}), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400

    if request.method == 'DELETE':
        """
            Function to remove the product.
        """
        if data.get('id', None) is not None:
            if ObjectId.is_valid(data['id']):
                delete_product = product_collection.delete_one({"_id": ObjectId(data['id'])})
                if delete_product.deleted_count == 1 :
                    return jsonify({ '_response': True, 'deleted': data['id'] }), 200
                else:
                    return jsonify({'_response': False, 'message': 'Data Not Found!'}), 400
            else:
                return jsonify({'_response': False, 'message': 'is not a valid ObjectId!'}), 400
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters!'}), 400


@product_api_blueprint.route('/api/product/image/', methods=['DELETE', 'PATCH'])
def imageproduct():
    data = request.get_json()
    product_collection = mongo.db.products
    if request.method == 'PATCH':
        """
            Function to update the image.
        """
        validated = validate_imgproduct_update(data)
        if validated['_response']:
            # product_collection.update({'_id': ObjectId(validated['data']['id'])}, { "$push": { "image":{'_id': ObjectId(), 'name': validated['data']['name']}}});
            # return jsonify({'_response': True, 'message': 'image updated'}), 200
            updateImageProduct = product_collection.update({'_id': ObjectId(validated['data']['id'])}, { "$push": { "image":{'_id': ObjectId(), 'file': validated['data']['file']}}})
            if updateImageProduct['updatedExisting'] is False:
                return jsonify({ '_response' : False, 'result': updateImageProduct }), 400
            return jsonify({'_response': True, 'message': 'product updated'}), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400

    if request.method == 'DELETE':
        """
            Function to remove the image.
        """
        if data.get('id', None) is not None:
            if data.get('img_id', None) is not None:
                if ObjectId.is_valid(data['id']) and ObjectId.is_valid(data['img_id']):
                    product_collection.update({'_id': ObjectId(data['id'])}, { "$pull": {"image":{'_id':ObjectId(data['img_id']) }}});
                    return jsonify({'_response': True, 'message': 'image deleted'}), 200
                else:
                    return jsonify({'_response': False, 'message': 'is not a valid ObjectId!'}), 400
            else:
                return jsonify({'_response': False, 'message': 'is not a valid ObjectId!'}), 400
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters!'}), 400



@product_api_blueprint.route('/api/categories', methods=['GET'])
def categories():
    """
    Function to fetch data.
    """
    try:
        category_collection = mongo.db.categories
        records_fetched = [category for category in category_collection.find().sort('created_at', -1)]  # show _id 
        # print(records_fetched)
        # records_fetched = [category for category in category_collection.find({}, {"_id":0})] ## hide _id
        return jsonify({'_response': True, 'categories': records_fetched}), 200
    except:
        return jsonify({ '_response': False, 'data': 'Not Found'}), 400

@product_api_blueprint.route('/api/category', methods=['POST', 'DELETE', 'PATCH'])
def category():
    data = request.get_json()
    category_collection = mongo.db.categories

    if request.method == 'POST':
        validated = validate_category(data)
        if validated['_response']:
            validated['data']['created_at'] = NOW

            category = category_collection.insert(validated['data'])
            return jsonify({'_response': True, 'data': category}), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400
            
    if request.method == 'PATCH':
        validated = validate_categories_update(data)
        if validated['_response']:
            
            updateCategory = category_collection.update({"_id": ObjectId(validated['data']['id'])}, {'$set': validated['data']['payload']})
            if updateCategory['updatedExisting'] is False:
                return jsonify({ '_response' : False, 'result': updateCategory }), 400
            return jsonify({'_response': True, 'message': 'category updated'}), 200
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400

    if request.method == 'DELETE':
        if data.get('id', None) is not None:
            if ObjectId.is_valid(data['id']):
                delete_cateogry = category_collection.delete_one({"_id": ObjectId(data['id'])})
                if delete_cateogry.deleted_count == 1 :
                    return jsonify({ '_response': True, 'deleted': data['id'] }), 200
                else:
                    return jsonify({'_response': False, 'message': 'Data Not Found!'}), 400
            else:
                return jsonify({'_response': False, 'message': 'is not a valid ObjectId!'}), 400
        else:
            return jsonify({'_response': False, 'message': 'Bad request parameters!'}), 400


@product_api_blueprint.route("/")
@product_api_blueprint.route("/api/")
def get_initial__response():
    """Welcome message for the API."""
    message = {
        'apiVersion': 'v1.0',
        'status': '200',
        'message': 'Welcome to the Product Service API'
    }

    resp = jsonify(message)
    return resp
