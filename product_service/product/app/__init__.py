import os
from flask import Flask
from .utils import initialize_db, CustomJSONEncoder

app = Flask(__name__)

app.config['MONGO_URI'] = os.environ.get('MONGO_URI')
app.config['JWT_SECRET_KEY'] = os.environ.get('JWT_SECRET')

initialize_db(app)
app.json_encoder = CustomJSONEncoder

from .api.routes import product_api_blueprint
app.register_blueprint(product_api_blueprint)