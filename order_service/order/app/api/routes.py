import os
from flask import Blueprint, request, jsonify
from flask_cors import CORS
from bson.objectid import ObjectId
from ..utils import mongo
from .schema import validate_addorder, validate_order

order_api_blueprint = Blueprint('order_api', __name__)
CORS(order_api_blueprint)

@order_api_blueprint.route("/")
@order_api_blueprint.route("/api/")
def get_initial__response():
    """Welcome message for the API."""
    message = {
        'apiVersion': 'v1.0',
        'status': '200',
        'message': 'Welcome to the Order Service API'
    }

    resp = jsonify(message)
    return resp

@order_api_blueprint.route('/api/orders', methods=['GET'])
def orders():
    """
    Function to fetch data.
    """
    try:
        order_collection = mongo.db.order
        getOrders = order_collection.aggregate([
            { '$lookup': { 'from': 'orderitem', 'localField': 'orderitem_id', 'foreignField': '_id', 'as': 'orderitem_info'}},
            { '$project': { '_id': 1, 'orderitem_id': 0, 'orderitem_info._id':0 }}
        ])
        records_fetched = [order for order in getOrders]
        return jsonify({'_response': True, 'order': records_fetched}), 200
    except:
        return jsonify({ '_response': False, 'data': 'Not Found'}), 400

@order_api_blueprint.route('/api/order/add_item', methods=['POST'])
def order_add_item():
    """
    Funtion to add item
    """
    # api_key = request.headers.get('Authorization')
    # response = UserClient.get_user(api_key)

    # if not response:
    #     return make_response(jsonify({'message': 'Not logged in'}), 401)
    # user = response['result']

    # create order collection
    order_collection = mongo.db.order
    # create order item collection
    orderitem_collection = mongo.db.orderitem

    data = request.get_json()
    validated = validate_addorder(data)
    
    if validated['_response']:
        known_order = order_collection.find_one({'user_id':validated['data']['user_id'], 'is_open': validated['data']['is_open']})
        have_list = True if len(list(known_order if known_order else [])) else False;

        OrderItemID = ObjectId()
        if have_list is False:
                  
            for item in validated['data']['items']:
                item.update({'_id': OrderItemID })

            # Create the order
            order = order_collection.insert({
                'user_id': validated['data']['user_id'],
                'is_open': True,
                'orderitem_id': [v['_id'] for v in validated['data']['items']]
            })
            # insert / order item list
            orderitem = orderitem_collection.insert(validated['data']['items'])

            return jsonify({'_response': True, 'data': order}), 200
        else:
            found = False
            # Check if we already have an order item with that product
            for item in known_order['orderitem_id']:
                getproductid = orderitem_collection.find_one({ '_id': ObjectId(item) })
                if getproductid['product_id'] == validated['data']['items'][0]['product_id']:
                    found = True
                    upQuantity = getproductid['quantity'] + int(validated['data']['items'][0]['quantity'])
                    orderitem_collection.update({'_id': ObjectId(item)}, { '$set': { 'quantity': upQuantity }});

            if found is False:
                for item in validated['data']['items']:
                    item.update({'_id': OrderItemID })
                orderitem = orderitem_collection.insert(validated['data']['items'])
                order_collection.update({'_id': ObjectId(known_order['_id'])}, {'$push':{ "orderitem_id": OrderItemID }})

            return jsonify({'_response': True, 'data': validated['data']['items']}), 200
    else:
        return jsonify({'_response': False, 'message': 'Bad request parameters: {}'.format(validated['message'])}), 400



@order_api_blueprint.route('/api/order', methods=['GET'])
def order():
    # api_key = request.headers.get('Authorization')
    # response = UserClient.get_user(api_key)

    # if not response:
    #     return make_response(jsonify({'message': 'Not logged in'}), 401)

    # user = response['result']

    data = request.get_json()
    validated = validate_order(data)

    order_collection = mongo.db.order
    open_order = order_collection.find_one({'user_id':validated['data']['user_id'], 'is_open': validated['data']['is_open']})
    have_order = True if len(list(open_order if open_order else [])) else False;
    
    if have_order is False:
        response = jsonify({'message': 'No order found'})
    else:
        orderitem = order_collection.aggregate([
            { '$match': { 'user_id': validated['data']['user_id'] } },
            { '$lookup': { 'from': 'orderitem', 'localField': 'orderitem_id', 'foreignField': '_id', 'as': 'orderitem_info'}},
            { '$project': { '_id': 1, 'orderitem_id': 0, 'orderitem_info._id':0 }}
        ])
        response = jsonify({'result': [v for v in orderitem]})

    return response


@order_api_blueprint.route('/api/order/checkout', methods=['POST'])
def checkout():
    # api_key = request.headers.get('Authorization')
    # response = UserClient.get_user(api_key)

    # if not response:
    #     return make_response(jsonify({'message': 'Not logged in'}), 401)

    # user = response['result']

    data = request.get_json()
    order_collection = mongo.db.order

    # try:
    getOrder = order_collection.update({'user_id': data['user_id'], 'is_open': True}, {'$set': {'is_open': False}})
    if getOrder['updatedExisting'] is False:
        response = jsonify({ '_response' : False, 'result': getOrder })
    
    return jsonify({ '_response' : True, 'result': getOrder })

@order_api_blueprint.errorhandler(404)
def page_not_found(e):
    """Send message to the user with notFound 404 status."""
    # Message to the user
    message = {
        "err":
            {
                "msg": "This route is currently not supported. Please refer API documentation."
            }
    }
    # Making the message looks good
    resp = jsonify(message)
    # Sending OK response
    resp.status_code = 404
    # Returning the object
    return resp