from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

addorder_schema = {
    "type": "object",
    "required": ["user_id", "is_open"],
    "properties": {
        "user_id": {
            "type": "string"
        },
        "items": {
            "type": "array",
            "items": { "$ref": "#/refOrderItem/orderitem" }
        },
        "is_open": {
            "type": "boolean"
        }
    },
    "refOrderItem": {
        "orderitem": {
            "type": "object",
            "required": [ "product_id", "quantity" ],
            "properties": {
                "product_id":{
                    "type": "string",
                    "description": "The id of product."
                },
                "quantity": {
                    "type": "integer",
                    "description": "The quantity of product."
                }
            }
        }
    },
    "additionalProperties": False
}

order_schema = {
    "type": "object",
    "required": ["user_id", "is_open"],
    "properties": {
        "user_id": {
            "type": "string"
        },
        "is_open": {
            "type": "boolean"
        }
    },
    "additionalProperties": False
}

def validate_addorder(data):
    try:
        validate(data, addorder_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}

def validate_order(data):
    try:
        validate(data, order_schema)
    except ValidationError as e:
        return {'_response': False, 'message': e}
    except SchemaError as e:
        return {'_response': False, 'message': e}
    return {'_response': True, 'data': data}
