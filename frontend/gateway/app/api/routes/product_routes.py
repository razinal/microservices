import json
from flask import Blueprint, request, jsonify, send_from_directory
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity
from ..services.ProductService import ProductService
from ..services.UserService import UserService
from app import app


product_gateway_blueprint = Blueprint('product_gateway', __name__)
CORS(product_gateway_blueprint)

@product_gateway_blueprint.route('/products', methods=['GET'])
@jwt_required
def products():
    productList =ProductService.get_products()
    return jsonify(productList), 200

@product_gateway_blueprint.route('/categories', methods=['GET'])
@jwt_required
def categories():
    categoryList =ProductService.get_categories()
    return jsonify(categoryList), 200


@product_gateway_blueprint.route('/product/<slug>', methods=['GET'])
@jwt_required
def productdetail(slug):
    productDetail = ProductService.get_product(slug)
    return jsonify(productDetail), 200

@product_gateway_blueprint.route('/product/', methods=['POST'])
@jwt_required
def postproduct():
    current_user = get_jwt_identity()
    productdata = request.form
    productfiles = request.files
    user_id = UserService.get_user_id(current_user)
    product = ProductService.post_product(productfiles, productdata, user_id)
    return jsonify(product), 200

@product_gateway_blueprint.route('/product/', methods=['DELETE'])
@jwt_required
def productdelete():
    productID = request.get_json()
    product = ProductService.delete_product(productID)
    return jsonify(product), 200

@product_gateway_blueprint.route('/product/', methods=['PUT'])
@jwt_required
def productupdate():
    productUp = request.get_json()
    product = ProductService.update_product(productUp)
    return jsonify(product), 200

@product_gateway_blueprint.route('/product/image/', methods=['POST'])
@jwt_required
def post_image_product():
    productImage = request.get_json()
    product = ProductService.post_product_image(productImage)
    return jsonify(product), 200

@product_gateway_blueprint.route('/product/image/', methods=['DELETE'])
@jwt_required
def delete_image_product():
    productImage = request.get_json()
    product = ProductService.delete_product_image(productImage)
    return jsonify(product), 200

# @product_gateway_blueprint.route('/media/<filename>')
# def display_image(filename):
# 	#print('display_image filename: ' + filename)
# 	return redirect(url_for('static', filename='uploaded/' + filename), code=301)



@product_gateway_blueprint.route("/media/<path:path>")
def get_file(path):
    return send_from_directory(app.config['UPLOAD_FOLDER'], path)

    # return send_from_directory(app.config['UPLOAD_FOLDER'], path, as_attachment=False)
    # return send_from_directory(app.config['UPLOAD_FOLDER'], path, as_attachment=True)

