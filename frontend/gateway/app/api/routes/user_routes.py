import json
from flask import Blueprint, request, jsonify
from flask_cors import CORS
from flask_jwt_extended import (
    create_access_token, 
    create_refresh_token,
    jwt_required, 
    jwt_refresh_token_required, 
    get_jwt_identity)
from ..services.UserService import UserService
from app import jwt

user_gateway_blueprint = Blueprint('user_gateway', __name__)
CORS(user_gateway_blueprint)

@user_gateway_blueprint.route('/login', methods=['POST'])
def login():
    auth = request.get_json()
    access_token = create_access_token(identity=auth)
    refresh_token = create_refresh_token(identity=auth)
    auth.update({'access_token': access_token, 'refresh_token': refresh_token})
    userAuth = UserService.post_login(auth)
    return jsonify(userAuth)


@user_gateway_blueprint.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    registerData = UserService.post_register(data)
    return jsonify(registerData)


@user_gateway_blueprint.route('/profile', methods=['GET'])
@jwt_required
def profile():
    current_user = get_jwt_identity()
    profileData = UserService.get_profile(current_user)
    return jsonify(profileData), 200


@user_gateway_blueprint.route('/user/update', methods=['PUT'])
@jwt_required
def update_profile():
    data_update = request.get_json()
    current_user = get_jwt_identity()
    userUpdate = UserService.update_profile(current_user,data_update)
    return jsonify(userUpdate)


@user_gateway_blueprint.route('/user/avatar', methods=['PUT'])
@jwt_required
def update_avatar():
    useravatar = request.files['avatar']
    current_user = get_jwt_identity()    
    user_id = UserService.get_user_id(current_user)
    userUpdate = UserService.update_avatar(user_id, current_user, useravatar)
    return jsonify(userUpdate)


@user_gateway_blueprint.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    ''' refresh token endpoint '''
    current_user = get_jwt_identity()
    ret = {
        'token': create_access_token(identity=current_user),
        'refresh': create_refresh_token(identity=current_user),
    }
    return jsonify({'_response': True, 'data': ret}), 200

@user_gateway_blueprint.route("/")
@user_gateway_blueprint.route("/api/")
def get_initial__response():
    """Welcome message for the API."""
    message = {
        'apiVersion': 'v1.0',
        'status': '200',
        'message': 'Welcome to the JakSewa Service API'
    }

    resp = jsonify(message)
    return resp


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401

@user_gateway_blueprint.errorhandler(404)
def page_not_found(e):
    """Send message to the user with notFound 404 status."""
    message = {
        "err":
            {
                "msg": "This route is currently not supported. Please refer API documentation."
            }
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp