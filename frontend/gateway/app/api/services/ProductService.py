import os
import requests
import pathlib
import datetime
from flask import request, json
from werkzeug.utils import secure_filename
from slugify import slugify
from app import app

class ProductService:
    @staticmethod
    def get_products():
        response = requests.get('http://product:5000/api/products')
        products = response.json()
        return products

    @staticmethod
    def get_categories():
        response = requests.get('http://product:5000/api/categories')
        categories = response.json()
        return categories

    @staticmethod
    def get_product(slug):
        response = requests.request(method="GET", url='http://product:5000/api/product/', json={"slug": slug})
        product = response.json()
        return product

    @staticmethod
    def post_product(productfiles, productdata, user_id):
        productdatapost = productdata.to_dict()
        productdatapost.update(harga=int(productdatapost['harga']))
        defaultpath = 'products/'+user_id+datetime.datetime.now().strftime("%d%m%y")+'/'+slugify(productdatapost['nama'])
        productimage = []
        for f in productfiles:
            file = request.files.getlist(f)
            for ff in file:
                productimage.append({'nama': defaultpath+'/'+secure_filename(ff.filename)})

        updatepost = {'user_id': user_id, 'slug': slugify(productdatapost['nama']), 'image': productimage}
        productdatapost.update(updatepost)

        url = 'http://product:5000/api/product/'
        response = requests.request("POST", url=url, json=productdatapost)
        getresponse = response.json()
        if getresponse['_response']:
            for f in productfiles:
                file = request.files.getlist(f)
                pathlib.Path(app.config['UPLOAD_FOLDER'], defaultpath).mkdir(parents=True, exist_ok=True)
                for ff in file:
                    filename = secure_filename(ff.filename)
                    ff.save(os.path.join(app.config['UPLOAD_FOLDER'], defaultpath, filename))
        else:
            getresponse = json.dumps({'_response': 'Error Upload'})
        return getresponse
        
    @staticmethod
    def update_product(product):
        url = 'http://product:5000/api/product/'
        product = requests.request("PATCH", url=url, json=product)
        return product.json()

    @staticmethod
    def delete_product(Id):
        url = 'http://product:5000/api/product/'
        product = requests.request("DELETE", url=url, json=Id)
        return product.json()
    
    @staticmethod
    def post_product_image(image):
        url = 'http://product:5000/api/product/image/'
        product = requests.request("PATCH", url=url, json=image)
        return product.json()

    @staticmethod
    def delete_product_image(image):
        url = 'http://product:5000/api/product/image/'
        product = requests.request("DELETE", url=url, json=image)
        return product.json()
