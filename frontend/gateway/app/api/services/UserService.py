import os
import requests
import pathlib
from app import app
from flask import request, json
from werkzeug.utils import secure_filename

class UserService:
    @staticmethod
    def post_login(auth):
        url = 'http://user:5000/api/user/auth'
        response = requests.request("POST", url=url, json=auth)
        return response.json()

    @staticmethod
    def get_user_id(current_user):
        print(current_user['email'])
        response = requests.request(method="GET", url='http://user:5000/api/userid', json={'email': current_user['email']})
        rsp = response.json()
        if rsp['_response']:
            return rsp['user']['_id']
        return 'Bad request parameters'

    @staticmethod
    def post_register(data):
        url = 'http://user:5000/api/user/'
        response = requests.request("POST", url=url, json=data)
        return response.json()

    @staticmethod
    def get_profile(current_user):
        response = requests.request(method="GET", url='http://user:5000/api/user/', json={'email': current_user['email']})
        return response.json()

    @staticmethod
    def update_profile(current_user, data_update):
        response = requests.request(method="PATCH", url='http://user:5000/api/user/', json={
            'email': current_user['email'], 
            'payload': data_update});
        return response.json()

    
    @staticmethod
    def update_avatar(user_id, current_user, useravatar):
        defaultpath = 'avatar/'+user_id
        userdatapost = {'avatar': defaultpath+'/'+secure_filename(useravatar.filename)}
        
        response = requests.request(method="PATCH", url='http://user:5000/api/user/', json={
            'email': current_user['email'], 
            'payload': userdatapost});
        
        getresponse = response.json()
        if getresponse['_response']:
            pathlib.Path(app.config['UPLOAD_FOLDER'], defaultpath).mkdir(parents=True, exist_ok=True)
            filename = secure_filename(useravatar.filename)
            useravatar.save(os.path.join(app.config['UPLOAD_FOLDER'], defaultpath, filename))
        else:
            getresponse = json.dumps({'_response': 'Error Upload'})
        return getresponse
        