import os
import json
import datetime

from flask import Flask
from flask_jwt_extended import JWTManager

app = Flask(__name__, static_url_path='/uploaded')

app.config.update(dict(
    JWT_SECRET_KEY=os.environ.get('FLASK_JWT_SECRET'),
    # JWT_ACCESS_TOKEN_EXPIRES=datetime.timedelta(minutes=1),
    JWT_ACCESS_TOKEN_EXPIRES=datetime.timedelta(days=1),
    JWT_REFRESH_TOKEN_EXPIRES=False,
    UPLOAD_FOLDER = os.environ.get('ROOT_PATH')+'/uploaded/'
))

jwt = JWTManager(app)

from .api.routes.user_routes import user_gateway_blueprint
from .api.routes.product_routes import product_gateway_blueprint

app.register_blueprint(user_gateway_blueprint)
app.register_blueprint(product_gateway_blueprint)