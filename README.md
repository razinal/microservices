**MicroServices**

Install Docker ([Download And Install](https://www.docker.com/get-started "Download And Install"))

Jalankan Docker, kemudia masuk ke dalam folder **frontend** dan jalankan perintah berikut:

`$ docker-compose -f docker-compose.deploy.yml up -d`

Pastikan semua container berjalan dengen sempurna dengan perintah berikut:

`$ docker ps`

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                 NAMES
2f2695d91dfd        frontend_product    "python -u setup.py"     6 weeks ago         Up 33 seconds       0.0.0.0:8083->5000/tcp                product_services_flask
fe9294b8e495        frontend_user       "python -u setup.py"     6 weeks ago         Up 33 seconds       0.0.0.0:8082->5000/tcp                user_services_flask
cf3d6d8166f5        frontend_order      "python -u setup.py"     6 weeks ago         Up 33 seconds       0.0.0.0:8084->5000/tcp                order_services_flask
5c63e413aa1c        frontend_gateway    "python -u setup.py"     6 weeks ago         Up 34 seconds       0.0.0.0:2020->5000/tcp                gateway_services
c3f87a6b264a        mongo:4.2.8         "docker-entrypoint.s…"   6 weeks ago         Up 34 seconds       27017/tcp, 0.0.0.0:27018->27018/tcp   user_services_mongodb
8de43d6ef148        mongo:4.2.8         "docker-entrypoint.s…"   6 weeks ago         Up 34 seconds       27017/tcp, 0.0.0.0:27020->27020/tcp   product_services_mongodb
bbbec421fc40        mongo:4.2.8         "docker-entrypoint.s…"   6 weeks ago         Up 34 seconds       27017/tcp, 0.0.0.0:27019->27019/tcp   order_services_mongodb
```

Untuk rebuild ulang services dan membuat ulang container :
`$ docker-compose -f docker-compose.deploy.yml build`

Untuk menjalankan services secara normal :
`$ docker-compose -f docker-compose.deploy.yml up -d`